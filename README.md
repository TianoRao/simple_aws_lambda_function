# Simple_AWS_Lambda_Function
# Brownian Motion Simulator

## Introduction
In this project, the author build a Brownian Motion simulator as a AWS lambda function based on Rust and cargo lambda.
Using this function we can input total time T and number of steps, and it will return a list of brownian motion increment. 
As a technical solution, the author utilized the function of AWS lambda, AWS API Gateway, data prerpocessing and Gitlab. 
This AWS Lambda function can be helpful for option pricing with Monte-carlo simultion, where we need the Brownian motion simulator to map stochastic processes.

## Diagram of Lambda Function
![Screenshot_2024-02-03_at_6.14.00_PM](/uploads/59088bdd57ab2ada90460b1015d13215/Screenshot_2024-02-03_at_6.14.00_PM.png)



## Test
![Screenshot_2024-02-03_at_6.14.12_PM](/uploads/41680fd4f2d9bdd17b76f4fd3b0d7aff/Screenshot_2024-02-03_at_6.14.12_PM.png)

In this tes, we can take the time = 5 and num_steps = 5 as inputs. Then it can return a list of 5 Brownian motion increment. 

## Setup
1. Install Rust and Cargo Lambda: `brew install rust`, `brew tap cargo-lambda/cargo-lambda`, `brew install cargo-lambda`    
2. Build a Cargo project:   
- Build your cargo lambda function based on the templet 
- Test locally with `watch` and `invoke`: Using these methods, we can test if the request works well and how the return is.  
3. AWS set-up:  
- Create an AWS account 
- AWS IAM web management: add users, attach polices, and set access keys.   
- AWS Lambda: release the function, and deploy.     
- AWS API Gateway: create an app, set API.  
4. Gitlab set-up:
- Add secrets: add AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION to gitlab secrets


## References
- https://stratusgrid.com/blog/aws-lambda-rust-how-to-deploy-aws-lambda-functions
- https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-create-api-as-simple-proxy-for-lambda.html
- https://www.cobbing.dev/blog/grappling-a-rust-lambda/
- https://www.cargo-lambda.info/commands/deploy.html
- https://coursera.org/groups/building-rust-aws-lambda-microservices-with-cargo-lambda-rd2pc
- https://github.com/DaGenix/rust-crypto/blob/master/examples/symmetriccipher.rs
